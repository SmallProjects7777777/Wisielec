import javax.sound.midi.Soundbank;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Wisielec {
    List<String> words = List.of("kot", "abrakadabra", "wisielec", "dom");
    String wordToGuess;
    char[] userWord;
    int lives = 3;

    public void play() {
        System.out.println("Witamy w grze wisielec :)");
        Scanner scanner = new Scanner(System.in);

        Random random = new Random();
        wordToGuess = words.get(random.nextInt(words.size()));

        userWord = new char[wordToGuess.length()];
        Arrays.fill(userWord,'_');

        while(!gameEnded()) {
            System.out.println("Twoja liczba żyć to: " + lives);
            System.out.println(userWord);
            System.out.println();
            System.out.println("Podaj kolejna litere");
            char letter = scanner.nextLine().charAt(0);
            checkLetter(letter);
        }

        if(lives == 0) {
            System.out.println("Niestety przegrałeś");
        } else {
            System.out.println("Brawo udało Ci się!!");
        }

        scanner.close();
    }

    private void checkLetter(char letter) {
        boolean foundLetter = false;

        for(int i = 0; i < wordToGuess.length(); i++) {
            if(wordToGuess.charAt(i) == letter) {
                userWord[i] = letter;
                foundLetter = true;
            }
        }

        if(!foundLetter) {
            System.out.println("Brak litery");
            lives--;
        }

    }

    private boolean gameEnded() {
        return lives == 0 || wordToGuess.equals(String.valueOf(userWord));
    }

}
